'use strict';

/**
 * @ngdoc overview
 * @name amhShop
 * @description # amhShop
 * 
 * Main module of the application.
 */
angular.module('amhShop', [
	'ngMaterialHomeBank',
	'ngMaterialHomeSpa',
	'mblowfish-language',
])//
.config(function($localStorageProvider) {
	$localStorageProvider.setKeyPrefix('amhShop.');
})
.run(function ($app, $toolbar, $sidenav) {
	// start the application
	$toolbar.setDefaultToolbars(['amh.owner-toolbar']);
	$sidenav.setDefaultSidenavs(['amh.main-sidenav', 'amh.cms.pages.sidenav']);
	$app.start('app-amh-shop');
})
.controller('MainCtrl', function () {
	// This controller may remove from the applications
});
